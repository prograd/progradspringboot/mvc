package com.example.StudentDetailsDisplay.controller;

import com.example.StudentDetailsDisplay.entities.Student;
import com.example.StudentDetailsDisplay.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Scanner;

@RestController
public class StudentController {
    @Autowired
    StudentService service;
    @GetMapping("/studentdisplay")
    public List<Student > display(){

        for (int iterate = 0; iterate < 2; iterate++) {
            Student student=new Student();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter name");
            student.setName(scanner.next());

            System.out.println("Enter age");
            student.setAge(scanner.nextInt());

            System.out.println("Enter Address");
            student.setAddress(scanner.next());
            service.addObj(student);
        }


         return service.getListOfStudent();
    }
}
