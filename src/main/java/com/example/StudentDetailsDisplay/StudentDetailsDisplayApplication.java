package com.example.StudentDetailsDisplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentDetailsDisplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentDetailsDisplayApplication.class, args);
	}

}
